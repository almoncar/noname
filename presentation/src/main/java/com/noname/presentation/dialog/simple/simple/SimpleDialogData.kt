package com.noname.presentation.dialog.simple.simple

import com.noname.domain.STRING_EMPTY
import es.babel.easymvvm.core.dialog.EmaDialogData

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */

data class SimpleDialogData(
    val title: String = STRING_EMPTY,
    override val proportionWidth: Float? = null,
    override val proportionHeight: Float? = null
) : EmaDialogData