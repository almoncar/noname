package com.noname.presentation.dialog.simple.simple

import android.view.View
import com.noname.presentation.R
import es.babel.easymvvm.android.ui.dialog.EmaBaseDialog

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */

class SimpleDialog : EmaBaseDialog<SimpleDialogData>() {

    override fun getLayout(): Int {
        return R.layout.dialog_simple
    }

    override fun setupData(data: SimpleDialogData, view: View) {

    }

}