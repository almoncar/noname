package com.noname.presentation.dialog.simple.simple

import es.babel.easymvvm.core.dialog.EmaDialogListener

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */

interface SimpleDialogListener : EmaDialogListener