
package com.noname.presentation.ui.splash

import android.os.Bundle
import com.noname.domain.STRING_EMPTY
import com.noname.presentation.R
import com.noname.presentation.base.BaseActivity


class SplashViewActivity : BaseActivity() {

    override fun getToolbarTitle(): String? {
        return STRING_EMPTY
    }

    override fun createActivity(savedInstanceState: Bundle?) {
        super.createActivity(savedInstanceState)
        hideToolbar()
    }

    override fun getNavGraph(): Int {
        return R.navigation.navigation_splash
    }
}