package com.noname.presentation.ui.splash

import com.noname.domain.STRING_EMPTY
import es.babel.easymvvm.core.state.EmaBaseState

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */

data class SplashState(val name:String = STRING_EMPTY) : EmaBaseState