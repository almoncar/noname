package com.noname.presentation.ui.splash

import androidx.navigation.NavController
import com.noname.presentation.base.BaseNavigator
import es.babel.easymvvm.core.navigator.EmaNavigationState


class SplashNavigator(override val navController: NavController) :
    BaseNavigator<SplashNavigator.Navigation>() {

    sealed class Navigation : EmaNavigationState
}