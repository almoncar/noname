package com.noname.presentation.ui.splash

import com.noname.presentation.R
import com.noname.presentation.base.BaseFragment
import es.babel.easymvvm.core.state.EmaExtraData
import org.kodein.di.generic.instance


class SplashViewFragment : BaseFragment<SplashState, SplashViewModel, SplashNavigator.Navigation>() {

    override val inputStateKey: String?
        get() = null

    override val viewModelSeed: SplashViewModel by instance()

    override val navigator: SplashNavigator by instance()

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_splash
    }

    override fun onInitialized(viewModel: SplashViewModel) {

    }

    override fun onNormal(data: SplashState) {
    }

    override fun onLoading(data: EmaExtraData) {
    }

    override fun onSingleEvent(data: EmaExtraData) {
    }

    override fun onError(error: Throwable): Boolean {
        return false
    }

}