package com.noname.presentation.base

import es.babel.easymvvm.android.navigation.EmaNavigator
import es.babel.easymvvm.core.navigator.EmaNavigationState

abstract class BaseNavigator<NS : EmaNavigationState> : EmaNavigator<NS> {

    sealed class NavigationBase : EmaNavigationState
}