package com.noname.presentation.injection

import androidx.fragment.app.Fragment
import com.noname.presentation.ui.splash.SplashViewModel
import es.babel.easymvvm.core.dialog.EmaDialogProvider
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */

fun generateFragmentModule(fragment: Fragment) = Kodein.Module(name = "FragmentModule") {

    bind<Fragment>() with provider { fragment }

    bind<SplashViewModel>() with provider {
        SplashViewModel()
    }

}
